# temporary build environment
# FROM node:12.2.0-alpine as build
# WORKDIR /quantem
# RUN apk add tree
# ENV PATH /app/node_modules/.bin:$PATH
# COPY package.json /quantem/package.json
# RUN npm install --silent
# #RUN npm install react-scripts@3.0.1 -g --silent
# COPY . /quantem
# # fixes 'win32-x64' binaries cannot be used on the 'linuxmusl-x64' platform
# RUN rm -rf ./node_modules/sharp/vendor/
# RUN npm install sharp
# RUN npm run build:online
# RUN tree ./app/build

# # create production image
# FROM nginx:1.16.1-alpine
# COPY --from=build /quantem/app/build /usr/share/nginx/html
# EXPOSE 80
# CMD ["nginx", "-g", "daemon off;"]

# temporary build environment
FROM node:12.2.0-alpine as build
WORKDIR /quantem
COPY package.json /quantem/package.json
COPY . /quantem
RUN apk add tree
RUN apk add --no-cache --virtual .gyp \
    python \
    && npm install \
    && apk del .gyp
RUN npm run build:online
RUN tree ./app/build
# create production image
FROM nginx:1.16.1-alpine
COPY --from=build /quantem/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

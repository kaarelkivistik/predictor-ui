#!/usr/bin/env bash
IMAGE=quantem/predictor_ui
NAME=predictor_ui
docker build -t ${IMAGE} .
docker stop ${NAME} && docker rm ${NAME} ; docker run -d -p 9000:80 --name ${NAME} ${IMAGE}

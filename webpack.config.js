const path = require('path');

const webpack               = require('webpack');
const StyleLintPlugin       = require('stylelint-webpack-plugin');
const MiniCssExtractPlugin  = require('mini-css-extract-plugin');
const SvgStorePlugin        = require('external-svg-sprite-loader');
const FractalModuleResolver = require('@gotoandplay/fractal-module-resolver-webpack-plugin');
const HtmlWebpackPlugin     = require('html-webpack-plugin');
const CopyWebpackPlugin     = require('copy-webpack-plugin');

const fractal           = require('./fractal.config.js');
const tsconfig          = require('./tsconfig.json');
const pkg               = require('./package.json');
const FractalPlugin     = require('./packages/fractal-webpack-plugin');
const TSConfigGenerator = require('./packages/tsconfig-generator-plugin');
const {prompt}          = require('enquirer');
const clients           = require('./clients.json');

/**
 * gotoAndReact class
 */
class gotoAndReact {
    constructor(env) {
        this.options = Object.assign({}, {
            production: false,
            styleguide: false,
            app: false,
            path: '/',
            client: env.CLIENT || '',
            syncFractal: !env.disableSync,
        }, env);
        this.env     = this.getEnv();

        if (!this.options.client) {
            return this.getClient();
        } else {
            return this.getCompilers();
        }
    }

    getEnv() {
        const env = Object.assign({}, require('./config.json'), {
            PRODUCTION: this.options.production ? 1 : 0,
        });

        Object.keys(env).map((key) => {
            env[key] = JSON.stringify(process.env[key] ? process.env[key] : env[key]);

            if (this.options.styleguide) {
                process.env[key] = env[key];
            }
        });

        return env;
    }

    async getClient() {
        const choices = [];
        for (let key in clients) {
            if (clients.hasOwnProperty(key)) {
                choices.push({
                    name: key,
                    message: clients[key].name,
                    value: key,
                });
            }
        }

        let answer = await prompt({
            type: 'select',
            name: 'client',
            message: 'Select client:',
            initial: 0,
            choices: choices
        });

        this.options.client = answer.client;

        return this.getCompilers();
    }

    getCompilers() {
        const compilers = [];
        this.client     = clients[this.options.client];
        if (this.client) {
            this.type   = this.options.client;
            if (this.options.styleguide) {
                compilers.push(this.getConfig('styleguide'));
            }

            if (this.options.app) {
                compilers.push(this.getConfig('app'));
            }

            return compilers;
        }
    }

    getPlugins(name) {
        const plugins = [
            new MiniCssExtractPlugin({
                filename: this.options.app && this.options.production ? 'css/[name].[chunkhash].css' : 'css/[name].css',
            }),
            new SvgStorePlugin(),
            new StyleLintPlugin(),
            new webpack.EnvironmentPlugin({
                webpack: true
            }),
            new webpack.DefinePlugin({
                'process.env': this.env,
            }),
        ];

        switch (name) {
            case 'app':
                plugins.push(new HtmlWebpackPlugin({
                    title: pkg.title,
                    template: './src/app/index.html',
                    hash: this.options.production,
                }));
                plugins.push(new CopyWebpackPlugin([
                    './src/app/public',
                ]));
                break;

            case 'styleguide':
                plugins.push(new TSConfigGenerator({
                    fractal: fractal,
                    tsConfig: tsconfig,
                    fileName: path.resolve(__dirname, path.join('tsconfig.json')),
                }));
                plugins.push(new FractalPlugin({
                    fractal: fractal,
                    isProduction: this.options.production,
                    chunksOrder: ['vendor', 'global'],
                }));
                break;
        }

        return plugins;
    }

    getDevServer(name) {
        let server = {};
        if (!this.options.production) {
            switch (name) {
                case 'app':
                    server = {
                        port: 80,
                        historyApiFallback: true,
                        contentBase: './src/app/',
                        open: true,
                        https: false,
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                            'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
                        },
                    };
                    break;
            }
        }

        return server;
    }

    getEntry(name) {
        let entry = {};
        switch (name) {
            case 'styleguide':
                entry = {
                    global: ['./src/patterns/index.fractal.ts']
                };
                break;

            case 'app':
                switch (this.type) {
                    case 'online':
                        entry = {
                            app: ['./src/app/index_online.tsx']
                        };
                        break;
                    case 'offline':
                        entry = {
                            app: ['./src/app/index_offline.tsx']
                        };
                        break;
                }
                break;
        }

        return entry;
    }

    getOutput(name) {
        let output = {};
        switch (name) {
            case 'styleguide':
                output = {
                    filename: 'js/[name].js',
                    path: path.resolve(__dirname, 'app/styleguide/public/inc'),
                    publicPath: this.options.production ? '../../inc/' : '/inc/',
                    library: '[name]',
                    libraryTarget: 'window'
                };
                break;

            case 'app':
                output = {
                    filename: this.options.production ? 'js/[name].[chunkhash].js' : 'js/[name].js',
                    path: path.resolve(__dirname, './app/build/'),
                    publicPath: this.options.path,
                };
                break;
        }

        return output;
    }

    getConfig(name) {
        return {
            watch: !this.options.production,
            mode: this.options.production ? 'production' : 'development',
            devtool: 'source-map',
            devServer: this.getDevServer(name),
            entry: this.getEntry(name),
            output: this.getOutput(name),
            plugins: this.getPlugins(name),
            resolve: {
                plugins: [
                    new FractalModuleResolver({
                        fractal: fractal,
                    }),
                ],
                extensions: ['.tsx', '.jsx', '.ts', '.js'],
            },
            optimization: {
                minimize: false,
                splitChunks: {
                    automaticNameDelimiter: '.',
                    cacheGroups: {
                        vendor: {
                            test: /[\\/]node_modules[\\/]/,
                            priority: -10,
                            chunks: 'all',
                        },
                        default: {
                            minChunks: 2,
                            priority: -20,
                            reuseExistingChunk: true
                        }
                    }
                }
            },
            module: {
                rules: [
                    {
                        test: /\.(ts|tsx)$/,
                        enforce: 'pre',
                        loader: 'tslint-loader'
                    },
                    {
                        test: /\.csv$/,
                        loader: 'csv-loader',
                        options: {
                            dynamicTyping: true,
                            header: true,
                            skipEmptyLines: true
                        }
                    },
                    {
                        test: /\.tsx?$/,
                        use: 'ts-loader',
                        exclude: /node_modules/
                    },
                    {
                        test: require.resolve('react'),
                        use: [{
                            loader: 'expose-loader',
                            options: 'React'
                        }]
                    },
                    {
                        test: require.resolve('react-dom'),
                        use: [{
                            loader: 'expose-loader',
                            options: 'ReactDOM'
                        }]
                    },
                    {
                        test: /\.(css|scss)$/,
                        use: [
                            {
                                loader: MiniCssExtractPlugin.loader,
                            },
                            {
                                loader: 'css-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'postcss-loader',
                                options: {
                                    sourceMap: true,
                                    plugins: [
                                        require('autoprefixer')()
                                    ]
                                }
                            },
                            {
                                loader: 'resolve-url-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    sourceMap: true
                                }
                            },
                            {
                                loader: 'sass-resources-loader',
                                options: {
                                    resources: [
                                        './src/patterns/core/variables.scss',
                                        './src/patterns/core/mixins.scss'
                                    ]
                                }
                            }
                        ],
                    },
                    {
                        test: /\.(svg)$/,
                        include: path.resolve(__dirname, 'src/patterns/components/icon/import/svg/'),
                        use: [{
                            loader: SvgStorePlugin.loader,
                            options: {
                                name: 'svg/icons.svg',
                                iconName: '[name]'
                            }
                        },
                            {
                                loader: 'svgo-loader',
                                options: {
                                    plugins: [
                                        {
                                            removeViewBox: false
                                        }
                                    ]
                                }
                            }]
                    },
                    {
                        test: /\.(woff|woff2)$/,
                        use: [{
                            loader: 'file-loader',
                            options: {
                                name: 'fonts/[name].[ext]'
                            }
                        }]
                    },
                    {
                        test: /\.js?$/,
                        use: [{
                            loader: 'file-loader',
                        }],
                        exclude: /node_modules/
                    },
                    {
                        test: /\.(png|svg|jpg|gif)$/,
                        exclude: path.resolve(__dirname, 'src/patterns/components/icon/import/svg/'),
                        use: [{
                            loader: 'file-loader',
                            options: {
                                name: 'img/[name].[ext]'
                            }
                        }]
                    },
                ]
            },
            stats: {
                modules: false,
                children: false,
            },
        };
    }

}

/**
 * Export config
 */
module.exports = (env) => {
    return new gotoAndReact(env);
};

import React from 'react';

import classNames from 'classnames';

let logoImage: string = '';

if (process.env.webpack) {
    logoImage = require('./import/svg/Quantem_logoasset_blue+orange.png');
    require('./logo.scss');
} else {
    logoImage = app.publicFolder + 'inc/img/Quantem_logoasset_blue+orange.png';
}

export interface ILogoProps {
    url?: string;
    modifier?: string;
    className?: string;
}

export default class Logo extends React.Component<ILogoProps, {}> {
    render(): JSX.Element {
        const LogoTag: 'a' | 'div' = this.props.url ? 'a' : 'div';
        const className: string = classNames('logo', this.props.modifier, this.props.className);

        return (
            <LogoTag
                className={className}
                href={this.props.url}
            >
                <img src={logoImage} alt="Logo" className="logo__img" />
            </LogoTag>
        );
    }
}

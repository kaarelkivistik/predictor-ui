import React from 'react';

import classNames from 'classnames';

if (process.env.webpack) {
    require('./main.scss');
}

export interface IMainProps {
    children: React.ReactNode;
    className?: string;
    elementRef?: React.Ref<HTMLDivElement>;
}

const Main: React.FC<IMainProps> = (props: IMainProps) => {
    const className: string = classNames(
        'main',
        props.className,
    );

    return (
        <main className={className} ref={props.elementRef}>
            {props.children}
        </main>
    );
};

export default Main;

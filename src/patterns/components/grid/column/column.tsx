import React from 'react';

import classNames from 'classnames';

export interface IGridColumnProps {
    className?: string;
    width?: string[];
    offset?: string[];
    order?: string[];
    align?: string[];
    noGutter?: boolean;
    children?: React.ReactNode;
    minWidth?: string;
    elementRef?: (ref: HTMLDivElement | null) => void;
}

export default class GridColumn extends React.Component<IGridColumnProps, {}> {
    render(): JSX.Element {
        const classArray: string[] = [];

        if (this.props.width) {
            for (const i of this.props.width) {
                classArray.push('grid__col--' + i);
            }
        } else {
            classArray.push('grid__col--xs-12');
        }

        if (this.props.offset) {
            for (const i of this.props.offset) {
                classArray.push('grid__col--offset-' + i);
            }
        }

        if (this.props.order) {
            for (const i of this.props.order) {
                classArray.push('grid__col--' + i);
            }
        }

        if (this.props.align) {
            for (const i of this.props.align) {
                classArray.push('grid__col--' + i);
            }
        }

        if (this.props.noGutter) {
            classArray.push('grid__col--no-vertical-gutter');
        }

        const className: string = classNames('grid__col', classArray.join(' '), this.props.className);

        return (
            <div className={className} style={{minWidth: this.props.minWidth}} ref={this.props.elementRef}>
                {this.props.children}
            </div>
        );
    }
}

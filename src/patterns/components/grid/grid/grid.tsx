import React from 'react';

import classNames from 'classnames';

if (process.env.webpack) {
    require('./grid.scss');
}

export interface IGridProps {
    className?: string;
    equalHeight?: boolean;
    reverse?: string[];
    align?: string[];
    noGutter?: string;
    children?: React.ReactNode;
    gutter?: 'small' | 'default';
}

export default class Grid extends React.Component<IGridProps, {}> {
    render(): JSX.Element {
        const classArray: string[] = [];

        if (this.props.equalHeight) {
            classArray.push('grid--equalheight');
        }

        if (this.props.reverse) {
            for (const i of this.props.reverse) {
                classArray.push('grid--reverse-' + i);
            }
        }

        if (this.props.align) {
            for (const i of this.props.align) {
                classArray.push('grid--' + i);
            }
        }

        if (this.props.noGutter) {
            switch (this.props.noGutter) {
                case 'all':
                    classArray.push('grid--no-gutter');
                    break;
                case 'vertical':
                    classArray.push('grid--no-vertical-gutter');
                    break;
                case 'horizontal':
                    classArray.push('grid--no-horizontal-gutter');
                    break;
            }
        }

        if (this.props.gutter) {
            classArray.push('grid--gutter-' + this.props.gutter);
        }

        const className: string = classNames('grid', classArray.join(' '), this.props.className);

        return (
            <div className={className}>
                {this.props.children}
            </div>
        );
    }
}

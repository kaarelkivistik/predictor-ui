import React from 'react';

import classNames from 'classnames';

import Fab from '@material-ui/core/Fab';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';

import GridColumn from '@column';
import Grid from '@grid';
import Icon from '@icon';

if (process.env.webpack) {
    require('./gradient-item.scss');
}

export interface IGradientItemProps {
    index: number;
    id: string;
    className?: string;
    time?: string;
    water?: string;
    organic?: string;
    showAdd?: boolean;
    showRemove?: boolean;
    onAddClick?: (event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => void;
    onRemoveClick?: () => void;
    onChange?: (name: keyof IGradientItemState, value: string, index: number) => void;
}

export interface IGradientItemState {
    time: string;
    water: string;
    organic: string;
}

export default class GradientItem extends React.Component<IGradientItemProps, IGradientItemState> {
    static defaultProps: IGradientItemProps = {
        index: 0,
        id: 'gradient-item-1',
        time: '0',
        water: '100',
        organic: '0',
    };

    constructor(props: IGradientItemProps) {
        super(props);

        this.state = {
            water: props.water || '100',
            organic: props.organic || '0',
            time: props.time || '0',
        };
    }

    handleChange = (name: keyof IGradientItemState) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const value: number = Number(event.target.value);
        let newValue: string = '';
        switch (name) {
            case 'water': {
                newValue = value <= 100 && value >= 0 ? event.target.value : value > 100 ? '100' : '0',

                    this.setState({
                        water: newValue,
                    });

                // set other fields value
                if ((value <= 100) && (value >= 0)) {
                    this.setState({
                        organic: (100 - value).toString(),
                    });

                    if (this.props.onChange) {
                        this.props.onChange('organic', (100 - value).toString(), this.props.index);
                    }
                }

                break;
            }
            case 'organic': {
                newValue = value <= 100 && value >= 0 ? event.target.value : value > 100 ? '100' : '0',

                    this.setState({
                        organic: newValue,
                    });

                // set other fields value
                if ((value <= 100) && (value >= 0)) {
                    this.setState({
                        water: (100 - value).toString(),
                    });

                    if (this.props.onChange) {
                        this.props.onChange('water', (100 - value).toString(), this.props.index);
                    }
                }

                break;
            }
            case 'time': {
                newValue = value > 0 || event.target.value === '' ? event.target.value : '0',

                    this.setState({
                        time: newValue,
                    });

                break;
            }
        }

        if (this.props.onChange) {
            this.props.onChange(name, newValue, this.props.index);
        }
    }

    handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
        if (event.target.value === '') {
            this.setState({
                time: '0',
            });
        }
    }

    render(): JSX.Element | null {
        const className: string = classNames(
            'gradient-item',
            this.props.className,
        );

        return (
            <div className={className}>
                <div className="gradient-item__inner">
                    <Grid align={['start-xs']} noGutter="all">
                        <GridColumn width={['xs']}>
                            <TextField
                                id={'gradient-time-' + this.props.id}
                                label="Time"
                                value={this.state.time}
                                onChange={this.handleChange('time')}
                                onBlur={this.handleBlur}
                                type="number"
                                className="gradient__field"
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">min</InputAdornment>,
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    hidden: this.props.index > 0,
                                }}
                                margin="none"
                            />
                        </GridColumn>
                        <GridColumn width={['xs']}>
                            <TextField
                                id={'gradient-water-' + this.props.id}
                                label="Water phase"
                                value={this.state.water}
                                onChange={this.handleChange('water')}
                                type="number"
                                className="gradient__field"
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">%</InputAdornment>,
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    hidden: this.props.index > 0,
                                }}
                                margin="none"
                            />
                        </GridColumn>
                        <GridColumn width={['xs']}>
                            <TextField
                                id={'gradient-organic-' + this.props.id}
                                label="Organic phase"
                                value={this.state.organic}
                                onChange={this.handleChange('organic')}
                                type="number"
                                className="gradient__field"
                                InputProps={{
                                    endAdornment: <InputAdornment position="end">%</InputAdornment>,
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    hidden: this.props.index > 0,
                                }}
                                margin="none"
                            />
                        </GridColumn>
                        <GridColumn width={['xs']}>
                            <Grid align={['start-xs']}>
                                <GridColumn width={['xs']} align={['start-xs']}>
                                    {this.props.showRemove && (
                                        <Fab size="small" aria-label="Remove" className="gradient__remove" onClick={this.props.onRemoveClick}>
                                            <Icon name="remove" />
                                        </Fab>
                                    )}
                                    {this.props.showAdd && (
                                        <Fab size="small" color="primary" aria-label="Add" onClick={this.props.onAddClick}>
                                            <Icon name="add" />
                                        </Fab>
                                    )}
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                    </Grid>
                </div>
            </div>
        );
    }

    static getDerivedStateFromProps(props: IGradientItemProps, _: IGradientItemState): IGradientItemState | null {
        if (props.time && props.water && props.organic) {
            return { time: props.time, water: props.water, organic: props.organic };
        }

        return null;
    }
}

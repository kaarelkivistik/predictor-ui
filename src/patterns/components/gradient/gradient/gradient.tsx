import React from 'react';

import classNames from 'classnames';

import GridColumn from '@column';
import GradientItem, { IGradientItemProps, IGradientItemState } from '@gradient-item';
import Grid from '@grid';

if (process.env.webpack) {
    require('./gradient.scss');
}

export interface IGradientProps {
    title: string;
    items?: IGradientItemProps[];
    values: IGradientItemState[];
    className?: string;
    onChange?: (name: keyof IGradientItemState, value: string, index: number) => void;
    handleAddClick?: (values: IGradientItemState[]) => void;
    handleRemoveClick?: (values: IGradientItemState[]) => void;
}

export default class Gradient extends React.Component<IGradientProps> {
    onAddClick = (): void => {
        if (this.props.handleAddClick) {
            const newValues: IGradientItemState[] = [...this.props.values];
            const time: string = this.props.values[this.props.values.length - 1].time;
            newValues.push({
                time: time,
                water: '100',
                organic: '0',
            });
            this.props.handleAddClick(newValues);
        }
    }

    onRemoveClick = (index: number): void => {
        if (this.props.handleRemoveClick) {
            const newValues: IGradientItemState[] = this.props.values.filter((_: IGradientItemState, i: number) => i !== index);

            this.props.handleRemoveClick(newValues);
        }
    }

    renderItems(): JSX.Element {
        const items: JSX.Element[] = this.props.values.map((item: IGradientItemState, index: number) => {
            return (
                <GridColumn width={['xs-12']} key={index}>
                    <GradientItem
                        {...item}
                        index={index}
                        onAddClick={this.onAddClick}
                        // tslint:disable-next-line: jsx-no-lambda
                        onRemoveClick={() => this.onRemoveClick(index)}
                        onChange={this.props.onChange}
                        showAdd={index + 1 === this.props.values.length}
                        showRemove={this.props.values.length > 1}
                    />
                </GridColumn>
            );
        });

        return (
            <Grid noGutter="horizontal" gutter="small">
                {items}
            </Grid>
        );
    }

    render(): JSX.Element {
        const className: string = classNames(
            'gradient',
            this.props.className,
        );

        return (
            <div className={className}>
                <div className="gradient__inner">
                    <Grid align={['start-xs']}>
                        <GridColumn width={['xs-3']}>
                            <Grid align={['center-xs']}>
                                <GridColumn width={['xs']}>
                                    <h3 className="gradient__title">{this.props.title}</h3>
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                        <GridColumn width={['xs-9']}>
                            {this.renderItems()}
                        </GridColumn>
                    </Grid>
                </div>
            </div>
        );
    }
}

import React from 'react';

import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Step, { StepProps } from '@material-ui/core/Step';
import StepLabel, { StepLabelProps } from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';

import classNames from 'classnames';

import Button, { IButtonProps } from '@button';
import GlobalNotification from '@global-notification';
import Gradient, { IGradientProps } from '@gradient';
import { IGradientItemProps, IGradientItemState } from '@gradient-item';
import Mode, { IModeProps } from '@mode';
import Solvent, { ISolventProps, ISolventState } from '@solvent';

import LoadTemplate, { ILoadTemplateProps } from '@m-load-template';
import SaveTemplate, { ISaveTemplateProps } from '@m-save-template';
import Step2, { IStep2Props } from '@m-step-2';
import Step3, { IStep3Props } from '@m-step-3';
import Step4, { IStep4Props } from '@m-step-4';

if (process.env.webpack) {
    require('./progress.scss');
}

export interface IProgressProps {
    loadTemplate: ILoadTemplateProps;
    saveTemplate: ISaveTemplateProps;
    solvent: ISolventProps;
    gradient: IGradientProps;
    mode: IModeProps;
    resetButton: IButtonProps;
    backButton: IButtonProps;
    finishButton: IButtonProps;
    nextButton: IButtonProps;
    skipButton: IButtonProps;
    steps: IProgressStepProps[];
    activeStep?: number;
    className?: string;
    optionalLabel?: string;
    skipped?: Set<number>;
    step2: IStep2Props;
    step3: IStep3Props;
    step4: IStep4Props;
    items?: IGradientItemProps[];
    onPrevClick?: (event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => void;
    onNextClick?: (event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => void;
    onResetClick?: () => void;
    handleSolventChange?: (name: keyof ISolventState, value: string) => void;
    handleGradientChange?: (name: keyof IGradientItemState, value: string, index: number) => void;
    handleGradientAdd?: () => void;
    onStepChange?: (step: number) => void;
    error?: string;
    success?: string;
}

export interface IProgressStepProps {
    label: string;
    isOptional?: boolean;
    mode?: IModeProps;
}

export interface IProgressState {
    activeStep: number;
    skipped: Set<number>;
    water?: string;
    organic?: string;
    ph?: string;
}

export default class Progress extends React.Component<IProgressProps, IProgressState> {
    constructor(props: IProgressProps) {
        super(props);

        this.state = {
            activeStep: props.activeStep || 0,
            skipped: props.skipped || new Set(),
        };
    }

    onSolventChange = (name: keyof ISolventState, value: string): void => {
        // tslint:disable-next-line:no-object-literal-type-assertion
        this.setState({
            [name]: value,
        } as Pick<ISolventState, keyof ISolventState>);

        if (this.props.handleSolventChange) {
            this.props.handleSolventChange(name, value);
        }
    }

    getStepContent = (step: number): JSX.Element => {
        switch (step) {
            case 0:
                return (
                    <div>
                        <LoadTemplate {...this.props.loadTemplate} />
                        <Divider />
                        <Mode {...this.props.mode} />
                        <Divider />
                        <Solvent {...this.props.solvent} onChange={this.onSolventChange} />
                        <Divider />
                        <Gradient {...this.props.gradient} />
                        <Divider />
                        <SaveTemplate {...this.props.saveTemplate} />
                    </div>
                );
            case 1:
                return (
                    <div>
                        <Step2 {...this.props.step2} />
                    </div>
                );
            case 2:
                return (
                    <div>
                        <Step3 {...this.props.step3} />
                    </div>
                );
            case 3:
                return (
                    <div>
                        <Step4 {...this.props.step4} />
                    </div>
                );
            default:
                return (
                    <div>
                        Unknown step
                    </div>
                );
        }
    }

    isStepSkipped = (step: number): boolean => {
        return this.state.skipped.has(step);
    }

    handleReset = () => {
        if (this.props.onResetClick) {
            this.props.onResetClick();
        }
    }

    handleBack = (event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => {
        this.setState((state: IProgressState) => ({
            activeStep: state.activeStep - 1 < 0 ? 0 : state.activeStep - 1,
        }), () => {
            if (this.props.onStepChange) {
                this.props.onStepChange(this.state.activeStep);
            }
        });

        if (this.props.onPrevClick) {
            this.props.onPrevClick(event);
        }
    }

    handleNext = (event: React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>) => {
        if (this.props.onNextClick) {
            this.props.onNextClick(event);
        }

        const { activeStep } = this.state;
        let { skipped } = this.state;

        if (this.isStepSkipped(activeStep)) {
            skipped = new Set(skipped.values());
            skipped.delete(activeStep);
        }

        this.setState({
            activeStep: activeStep + 1,
            skipped,
        }, () => {
            if (this.props.onStepChange) {
                this.props.onStepChange(this.state.activeStep);
            }
        });
    }

    handleSkip = () => {
        const { activeStep } = this.state;

        this.setState((state: IProgressState) => {
            const skipped: Set<number> = new Set(state.skipped.values());
            skipped.add(activeStep);

            return {
                activeStep: state.activeStep + 1,
                skipped,
            };
        });
    }

    render(): JSX.Element {
        const className: string = classNames('progress', this.props.className);
        const nextButtonProps: IButtonProps = this.state.activeStep === this.props.steps.length - 1 ? this.props.finishButton : this.props.nextButton;

        return (
            <div className={className}>
                <Stepper activeStep={this.state.activeStep} className="progress__stepper" alternativeLabel={true}>
                    {this.props.steps.map((item: IProgressStepProps, index: number) => {
                        const props: StepProps = {};
                        const labelProps: StepLabelProps = {
                            children: {},
                        };

                        if (item.isOptional && this.props.optionalLabel) {
                            labelProps.optional = this.props.optionalLabel;
                        }

                        if (this.isStepSkipped(index)) {
                            props.completed = false;
                        }

                        return (
                            <Step className="progress__step" key={item.label} {...props}>
                                <StepLabel {...labelProps}>{item.label}</StepLabel>
                            </Step>
                        );
                    })}
                </Stepper>
                <div className="progress__content">
                    <Paper>

                        {this.getStepContent(this.state.activeStep)}
                        <div className="progress__actions">
                            {this.state.activeStep > 0 &&
                                (
                                    <Button
                                        {...this.props.backButton}
                                        type="button"
                                        isDisabled={this.state.activeStep === 0}
                                        onClick={this.handleBack}
                                        color="ghost"
                                        className="progress__action-button progress__action-button--back"
                                        iconLeft="chevron-left"
                                    />
                                )
                            }
                            {this.state.activeStep === 3 &&
                                (
                                    <Button
                                        {...this.props.resetButton}
                                        type="button"
                                        onClick={this.handleReset}
                                        color="ghost"
                                        className="progress__action-button progress__action-button--reset"
                                        iconLeft="refresh"
                                    />
                                )
                            }
                            {this.state.activeStep !== 2 &&
                                (
                                    <Button
                                        type="button"
                                        onClick={this.handleNext}
                                        className="progress__action-button progress__action-button--next"
                                        iconRight="chevron-right"
                                        {...nextButtonProps}
                                    />
                                )
                            }
                        </div>

                    </Paper>
                </div>
                {this.props.error && <GlobalNotification message={this.props.error} />}
                {this.props.success && <GlobalNotification message={this.props.success} severity="success" />}
            </div>
        );
    }

    static getDerivedStateFromProps(props: IProgressProps, state: IProgressState): IProgressState | null {
        if (typeof props.activeStep !== 'undefined' && props.activeStep !== state.activeStep) {
            return {
                activeStep: props.activeStep,
                skipped: state.skipped,
            };
        }

        return null;
    }
}

import React, { MutableRefObject, useEffect, useRef, useState } from 'react';

import classNames from 'classnames';

import Button, { IButtonProps, TButtonEvent } from '@button';
import GridColumn from '@column';
import Container from '@container';
import Grid from '@grid';
import Helpers from '@helpers';
import Modal from '@modal';

if (process.env.webpack) {
    require('./cookie-notice.scss');
}

export interface ICookieNoticeProps {
    acceptButton: IButtonProps;
    children: JSX.Element;
    modalContent: JSX.Element;
    moreButton: IButtonProps;
    className?: string;
}

export interface ICookieNoticeState {
    isOpen: boolean;
    isVisible: boolean;
    isMounted: boolean;
}

const CookieNotice: React.FC<ICookieNoticeProps> = (props: ICookieNoticeProps) => {
    const timeout: MutableRefObject<number> = useRef(0);
    const [isOpen, setIsOpen] = useState(false);
    const [isVisible, setIsVisible] = useState(false);
    const [isAnimatingOut, setIsAnimatingOut] = useState(false);
    const className: string = classNames(
        'cookie-notice',
        {
            'is-animating-out': isAnimatingOut,
        },
    );

    useEffect(() => {
        if (Helpers.getCookie('COOKIES_ACCEPTED') === undefined) {
            setIsVisible(true);
        } else {
            setIsVisible(false);
        }
    }, []);

    const handleModalClose: () => void = () => {
        setIsOpen(false);
    };

    const showModal: (e: TButtonEvent) => void = (e: TButtonEvent): void => {
        e.preventDefault();

        setIsOpen(true);

        if (props.moreButton.onClick) {
            props.moreButton.onClick(e);
        }
    };

    const handleAccept: (e: TButtonEvent) => void = (e: TButtonEvent): void => {
        e.preventDefault();

        Helpers.setCookie('COOKIES_ACCEPTED', 'TRUE', 30);
        if (timeout.current) {
            window.clearTimeout(timeout.current);
        }
        setIsAnimatingOut(true);
        timeout.current = window.setTimeout(() => setIsVisible(false), 1000);

        if (props.acceptButton.onClick) {
            props.acceptButton.onClick(e);
        }
    };

    if (isVisible) {
        return (
            <React.Fragment>
                <div className={className}>
                    <Container>
                        <Grid align={['end-xs']}>
                            <GridColumn width={['xs-12']}>
                                {props.children}
                            </GridColumn>
                            <GridColumn width={['xs']}>
                                <Button
                                    {...props.moreButton}
                                    color="ghost"
                                    iconRight="next"
                                    onClick={showModal}
                                    url="#cookie-modal"
                                />
                            </GridColumn>
                            <GridColumn width={['xs']}>
                                <Button
                                    {...props.acceptButton}
                                    color="ghost"
                                    iconRight="check"
                                    onClick={handleAccept}
                                />
                            </GridColumn>
                        </Grid>
                    </Container>
                </div>
                <Modal
                    id="cookie-modal"
                    isOpen={isOpen}
                    onClose={handleModalClose}
                >
                    {props.modalContent}
                </Modal>
            </React.Fragment>
        );
    } else {
        return null;
    }
};

export default CookieNotice;

import React, { useEffect } from 'react';

import classNames from 'classnames';

import Snackbar from '@material-ui/core/Snackbar';
import { AlertProps, Color, default as MuiAlert } from '@material-ui/lab/Alert';

if (process.env.webpack) {
    require('./global-notification.scss');
}

export interface IGlobalNotificationProps {
    message: string;
    className?: string;
    severity?: Color;
}

const Alert: React.FC<AlertProps> = (props: AlertProps) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const GlobalNotification: React.FC<IGlobalNotificationProps> = (props: IGlobalNotificationProps) => {
    const [open, setOpen] = React.useState(true);
    const className: string = classNames(
        'global-notification',
        {
            [`global-notification--severity-${props.severity}`]: props.severity,
        },
        props.className,
    );

    useEffect(() => {
        setOpen(true);
    }, [props.message]);

    const handleClose: (event: React.SyntheticEvent, reason?: string) => void = ({ }: {}, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    return (
        <Snackbar
            autoHideDuration={5000}
            anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
            }}
            className={className}
            onClose={handleClose}
            open={open}
        >
            <Alert onClose={handleClose} severity={props.severity}>
                {props.message}
            </Alert>
        </Snackbar>
    );
};

GlobalNotification.defaultProps = {
    severity: 'error',
};

export default GlobalNotification;

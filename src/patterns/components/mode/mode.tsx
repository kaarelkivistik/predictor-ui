import React from 'react';

import classNames from 'classnames';

import ChoiceGroup, { IChoiceGroupProps } from '@choice-group';
import GridColumn from '@column';
import Grid from '@grid';

if (process.env.webpack) {
    require('./mode.scss');
}

export type TEsiMode = 'positive' | 'negative';

export interface IModeProps {
    title: string;
    choiceGroup: IChoiceGroupProps;
    className?: string;
    mode?: TEsiMode;
}

export default class Mode extends React.Component<IModeProps, {}> {
    render(): JSX.Element {
        const className: string = classNames(
            'mode',
            this.props.className,
        );

        return (
            <div className={className}>
                <div className="mode__inner">
                    <Grid align={['start-xs']}>
                        <GridColumn width={['xs-3']}>
                            <Grid align={['center-xs']}>
                                <GridColumn width={['xs']}>
                                    <h3 className="mode__title">{this.props.title}</h3>
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                        <GridColumn width={['xs-3']}>
                            <Grid>
                                <GridColumn width={['xs-12']}>
                                    <ChoiceGroup {...this.props.choiceGroup} hiddenLabel={true} value={this.props.mode} />
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                    </Grid>
                </div>
            </div>
        );
    }
}

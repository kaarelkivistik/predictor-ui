import React from 'react';

import classNames from 'classnames';

import Icon from '@icon';

if (process.env.webpack) {
    require('./button.scss');
}

export interface IButtonProps {
    text: string | React.ReactNode;
    attributes?: React.HTMLProps<HTMLButtonElement> | React.HTMLProps<HTMLAnchorElement>;
    className?: string;
    color?: 'default' | 'ghost';
    elementRef?: (element: HTMLElement | null) => void;
    iconLeft?: string;
    iconRight?: string;
    isDisabled?: boolean;
    onClick?: (event: TButtonEvent) => void;
    target?: 'self' | '_blank';
    type?: 'button' | 'submit' | 'reset';
    url?: string;
}

export type TButtonEvent = React.MouseEvent<HTMLButtonElement | HTMLAnchorElement>;

const Button: React.FC<IButtonProps> = (props: IButtonProps) => {
    const ButtonTag: 'a' | 'button' = props.url ? 'a' : 'button';
    const className: string = classNames(
        'button',
        {
            'is-disabled': props.isDisabled,
            'button--icon-right': props.iconRight,
            'button--icon-left': props.iconLeft,
            [`button--style-${props.color}`]: props.color,
        },
        props.className,
    );

    const renderIcon: (name: string, position?: 'left' | 'right') => JSX.Element = (name: string, position?: 'left' | 'right') => {
        const iconClassName: string = classNames(
            'button__icon',
            {
                [`button__icon--${position}`]: position,
            },
        );

        return (
            <Icon className={iconClassName} name={name} />
        );
    };

    return (
        <ButtonTag
            className={className}
            onClick={props.onClick}
            type={props.url ? undefined : props.type}
            href={props.url}
        >
            {props.iconLeft && renderIcon(props.iconLeft, 'left')}
            <span className="button__text">
                {props.text}
            </span>
            {props.iconRight && renderIcon(props.iconRight, 'right')}
        </ButtonTag>
    );
};

export default Button;

import React, { useEffect, useState } from 'react';

import classNames from 'classnames';
import Papa, { ParseError, ParseResult } from 'papaparse';
import { DropzoneProps, useDropzone } from 'react-dropzone';

import GlobalNotification from '@global-notification';
import Icon from '@icon';

if (process.env.webpack) {
    require('./file-drop.scss');
}

export interface IFileDropProps extends DropzoneProps {
    activeLabel: string;
    label: string;
    className?: string;
    onChange?: (e: React.FormEvent<HTMLInputElement>) => void;
    onDrop?: (acceptedFiles?: File[], rejectedFiles?: File[]) => void;
    changeHandle?: (result: IParsedFile[]) => void;
}

export interface ICsvProps {
    SMILES: string;
    retTime: number;
    signal: number;
    concentration?: number;
}

export interface IParsedFile {
    fileName: string;
    data: ICsvProps[];
}

const FileDrop: React.FC<IFileDropProps> = (props: IFileDropProps) => {
    const [result, setResult] = useState<IParsedFile[]>([]);
    const [error, setError] = useState<string | undefined>();
    const onDrop: (acceptedFiles: File[], rejectedFiles: File[]) => void = (acceptedFiles: File[], rejectedFiles: File[]): void => {
        if (acceptedFiles.length > 0) {
            parseCsv(acceptedFiles);
        }

        if (rejectedFiles.length > 0) {
            const getFileNames: () => string = () => {
                let fileNames: string = '';

                rejectedFiles.forEach((file: File) => {
                    fileNames += file.name + ' ';
                });

                return fileNames;
            };

            setError('Rejected files: ' + getFileNames());
        }

        if (props.onDrop) {
            props.onDrop(acceptedFiles, rejectedFiles);
        }
    };

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        maxSize: 1000000, // Maximum file size in bytes (10MB)
        onDrop,
        multiple: true,
        accept: '.csv',
    });

    const className: string = classNames(
        'file-drop',
        {
            'is-active': isDragActive,
        },
        props.className,
    );

    useEffect(() => {
        if (props.changeHandle) {
            props.changeHandle(result);
        }
    }, [result]);

    const parseCsv: (files: File[]) => Promise<void> = async(files: File[]) => {
        Promise.all(files.map((file: File) => {
            return new Promise<IParsedFile>((resolve: (value: IParsedFile) => void, reject: (value: string) => void) => {
                Papa.parse(file, {
                    // tslint:disable-next-line:no-any
                    complete: (results: ParseResult<any>) => {
                        const parsedCsv: ICsvProps[] = [];

                        results.data.forEach((line: string, index: number) => {
                            // Skip headers
                            if (index > 0) {
                                parsedCsv.push({
                                    SMILES: line[0],
                                    retTime: parseFloat(line[1]),
                                    signal: parseFloat(line[2]),
                                    concentration: (line.length > 3 ? parseFloat(line[3]) : undefined),
                                });
                            }
                        });

                        resolve({ fileName: file.name, data: parsedCsv });
                    },
                    error: (err: ParseError) => {
                        reject(err.message);
                    },
                    skipEmptyLines: true,
                });
            });
        })).then((parsedFiles: IParsedFile[]) => {
            setResult([...result, ...parsedFiles]);
        }).catch((err: Error) => setError(err.message));

    };

    const onClickRemove: (e: React.MouseEvent, fileName: string) => void = (e: React.MouseEvent, fileName: string): void => {
        e.stopPropagation();
        e.preventDefault();
        const newResult: IParsedFile[] = result.filter((r: IParsedFile) => r.fileName !== fileName);
        setResult(newResult);
    };

    const renderLabel: () => JSX.Element = () => {
        return (
            <ul className="file-drop__label">
                {result.length > 0
                    ? result.map(({ fileName }: IParsedFile, i: number) => (
                        <li key={i} className="file-drop__label-text">
                            <b>{fileName}</b>
                            {/* tslint:disable-next-line: jsx-no-lambda */}
                            <button onClick={(e: React.MouseEvent<HTMLButtonElement>) => onClickRemove(e, fileName)} className="file-drop__remove">
                                <Icon modifier="md" name="close">Remove</Icon>
                            </button>
                        </li>
                    ))
                    : <p className="file-drop__label-text">{props.label}</p>}
            </ul>
        );
    };

    return (
        <React.Fragment>
            <div className={className}>
                <div {...getRootProps()} className="file-drop__dropzone">
                    <input {...getInputProps()} />
                    {isDragActive ?
                        <div className="file-drop__label">{props.activeLabel}</div> :
                        renderLabel()
                    }
                </div>
            </div>
            {error && <GlobalNotification message={error} />}
        </React.Fragment>
    );
};

export default FileDrop;

import React from 'react';

import classNames from 'classnames';

if (process.env.webpack) {
    require('./navigation.scss');
}

export interface INavigationItemProps {
    url: string;
    label: string;
    disabled?: boolean;
    onClick?: (event: React.MouseEvent<HTMLAnchorElement>) => void;
    current?: boolean;
}

export interface INavigationProps {
    items: INavigationItemProps[];
    className?: string;
}

const NavigationItem: React.FC<INavigationItemProps> = (props: INavigationItemProps) => {
    const className: string = classNames(
        'navigation__item',
        {
            'is-current': props.current,
            'is-disabled': props.disabled,
        },
    );

    return (
        <li className={className}>
            <a
                className="navigation__link"
                href={props.url}
                onClick={props.onClick}
            >
                {props.label}
            </a>
        </li>
    );
};

const Navigation: React.FC<INavigationProps> = (props: INavigationProps) => {
    const className: string = classNames('navigation', props.className);

    const handleClick:
        (item: INavigationItemProps, event: React.MouseEvent<HTMLAnchorElement>) => void =
        (item: INavigationItemProps, event: React.MouseEvent<HTMLAnchorElement>) => {
        if (item.onClick) {
            item.onClick(event);
        }
    };

    const renderItems: () => JSX.Element[] = (): JSX.Element[] => {
        return props.items.map((item: INavigationItemProps, index: number) => {
            return <NavigationItem {...item} key={index} onClick={handleClick.bind(null, item)} />;
        });
    };

    return (
        <nav className={className}>
            <ul className="navigation__list">
                {renderItems()}
            </ul>
        </nav>
    );
};

export default Navigation;

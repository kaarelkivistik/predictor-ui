import React from 'react';

import classNames from 'classnames';

import GridColumn from '@column';
import Grid from '@grid';

import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

if (process.env.webpack) {
    require('./solvent.scss');
}

export interface ISolventProps {
    title: string;
    waterTitle: string;
    waterOptions?: ISolventSelectProps[];
    organicTitle: string;
    organicOptions?: ISolventSelectProps[];
    className?: string;
    water?: string;
    organic?: string;
    ph?: string;
    onChange?: (name: keyof ISolventState, value: string) => void;
}

export interface ISolventSelectProps {
    label: string;
    value: string;
}

export interface ISolventState {
    water?: string;
    organic?: string;
    ph: string;
}

export default class Solvent extends React.Component<ISolventProps, ISolventState> {
    constructor(props: ISolventProps) {
        super(props);

        this.state = {
            water: props.water || ((typeof props.waterOptions !== 'undefined') ? props.waterOptions[0].value : undefined),
            organic: props.organic || ((typeof props.organicOptions !== 'undefined') ? props.organicOptions[0].value : undefined),
            ph: props.ph || '7',
        };
    }

    handleChange = (name: keyof ISolventState) => (event: React.ChangeEvent<HTMLInputElement>) => {
        if (name === 'ph') {
            const newPh: string = (parseFloat(event.target.value) > 14) ? '14' : parseFloat(event.target.value) < 0 ? '0' : event.target.value;

            this.setState({
                ph: newPh,
            });

            if (this.props.onChange) {
                this.props.onChange('ph', newPh);
            }
        } else {
            // tslint:disable-next-line:no-object-literal-type-assertion
            this.setState({
                [name]: event.target.value,
            } as Pick<ISolventState, keyof ISolventState>);

            if (this.props.onChange) {
                this.props.onChange(name, event.target.value);
            }
        }

    }

    render(): JSX.Element {
        const className: string = classNames(
            'solvent',
            this.props.className,
        );

        return (
            <div className={className}>
                <div className="solvent__inner">
                    <Grid align={['start-xs']}>
                        <GridColumn width={['xs-3']}>
                            <Grid align={['center-xs']}>
                                <GridColumn width={['xs']}>
                                    <h3 className="solvent__title">{this.props.title}</h3>
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                        <GridColumn width={['xs-3']}>
                            <Grid>
                                <GridColumn width={['xs-12']}>
                                    <TextField
                                        id="waterSelect"
                                        select={true}
                                        label={this.props.waterTitle}
                                        className="solvent__field"
                                        value={this.state.water}
                                        onChange={this.handleChange('water')}
                                        SelectProps={{
                                            MenuProps: {
                                                className: 'solvent__field-select',
                                            },
                                        }}
                                        margin="none"
                                    >
                                        {this.props.waterOptions && this.props.waterOptions.map((option: ISolventSelectProps) => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </GridColumn>
                                <GridColumn width={['xs-12']}>
                                    <TextField
                                        id="standard-number"
                                        label="pH"
                                        value={this.state.ph}
                                        onChange={this.handleChange('ph')}
                                        type="number"
                                        className="solvent__field"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        helperText="Enter a value from 0 to 14."
                                        margin="none"
                                    />
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                        <GridColumn width={['xs-3']}>
                            <Grid>
                                <GridColumn width={['xs-12']}>
                                    <TextField
                                        required={true}
                                        id="organicSelect"
                                        select={true}
                                        label={this.props.organicTitle}
                                        className="solvent__field"
                                        value={this.state.organic}
                                        onChange={this.handleChange('organic')}
                                        SelectProps={{
                                            MenuProps: {
                                                className: 'solvent__field-select',
                                            },
                                        }}
                                        margin="none"
                                    >
                                        {this.props.organicOptions && this.props.organicOptions.map((option: ISolventSelectProps) => (
                                            <MenuItem key={option.value} value={option.value}>
                                                {option.label}
                                            </MenuItem>
                                        ))}
                                    </TextField>
                                </GridColumn>
                            </Grid>
                        </GridColumn>
                    </Grid>
                </div>
            </div>
        );
    }

    static getDerivedStateFromProps(props: ISolventProps, state: ISolventState): ISolventState | null {
        return {
            organic: props.organic || state.organic,
            ph: props.ph || state.ph,
            water: props.water || state.water,
        };
    }
}

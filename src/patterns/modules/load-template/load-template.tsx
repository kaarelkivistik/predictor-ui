import React from 'react';

import Accordion from '@material-ui/core/Accordion';
import AccordionActions from '@material-ui/core/AccordionActions';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import MButton from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

import Button from '@button';
import GridColumn from '@column';
import Grid from '@grid';
import Icon from '@icon';
import { Chip, Typography } from '@material-ui/core';
import Modal from '@modal';
import { TEsiMode } from '@mode';

if (process.env.webpack) {
    require('./load-template.scss');
}

export interface ITemplate {
    id: number;
    default: boolean;
    name: string;
    date: string;
    gradientData: {
        ESImode: TEsiMode;
        organicModifier: string;
        waterPhaseAdditive: string;
        pH: string;
        steps: IGradientStep[],
        NH4: number;
    };
}

interface IGradientStep {
    startTime: number;
    organicPhaseModifierPercent: number;

}

export interface ILoadTemplateProps {
    templates: ITemplate[];
    onSelect?: (value: string) => void;
    onRemove?: (name: string) => void;
    onSetDefault: (name: string) => void;
    selectedTemplate: string;
}

export interface ILoadTemplateSelectOption {
    label: string;
    value: string;
}

export default function LoadTemplate(props: ILoadTemplateProps): JSX.Element {
    const [isModalOpen, setModalOpen] = React.useState(false);
    const handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (props.onSelect) {
            props.onSelect(e.target.value);
        }
    };

    const onClickManage: () => void = () => {
        setModalOpen(true);
    };

    const handleModalClose: () => void = () => {
        setModalOpen(false);
    };

    const handleRemove: (name: string) => void = (name: string) => {
        const confirmed: boolean = confirm('Are you sure you want to delete template ' + name + '?');

        if (confirmed && props.onRemove) {
            props.onRemove(name);
        }
    };

    const handleSetDefault: (name: string) => void = (name: string) => {
        if (props.onSetDefault) {
            props.onSetDefault(name);
        }
    };

    const getTemplatesList: () => JSX.Element[] | string = () => {
        if (props.templates.length > 0) {
            return props.templates.map((temp: ITemplate) => (
                <Accordion key={temp.id} className="load-template__item">
                    <AccordionSummary expandIcon={<Icon name="expand-more" />}>
                        <div className="load-template__item-header">
                            <h4 className="load-template__item-heading">
                                <b>{temp.name}</b>
                            </h4>

                            {temp.default && <Chip size="small" label="Default" />}
                            <div className="text-light">
                                {new Date(temp.date).toLocaleDateString()}
                            </div>
                        </div>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography component="div">
                            <p><b>ESI mode:</b> {temp.gradientData.ESImode}</p>
                            <p><b>Organic Phase:</b> {temp.gradientData.organicModifier}</p>
                            <p><b>Water Phase Additive:</b> {temp.gradientData.waterPhaseAdditive}</p>
                            <p><b>pH:</b> {temp.gradientData.pH}</p>
                        </Typography>
                    </AccordionDetails>
                    <Divider />
                    <AccordionDetails>
                        <Typography component="div">
                            <b>Gradient:</b>
                            <ul>
                                {temp.gradientData.steps.map((step: IGradientStep, i: number) =>
                                    <li key={i}>- Start time: {step.startTime}min, Organic phase: {step.organicPhaseModifierPercent}%</li>)}
                            </ul>
                        </Typography>
                    </AccordionDetails>
                    <Divider />
                    <AccordionActions className="load-template__item-action">
                        {/* tslint:disable-next-line: jsx-no-lambda */}
                        <MButton size="small" color="secondary" onClick={() => handleRemove(temp.name)}>
                            Delete
                        </MButton>
                        {/* tslint:disable-next-line: jsx-no-lambda */}
                        <MButton disabled={temp.default} size="small" color="primary" onClick={() => handleSetDefault(temp.name)}>
                            Set default
                        </MButton>
                    </AccordionActions>
                </Accordion>
            ));
        }

        return 'You don\'t have any saved templates';
    };

    return (
        <div className="load-template">
            <Grid align={['start-xs']}>
                <GridColumn width={['xs-3']}>
                    <Grid align={['center-xs']}>
                        <GridColumn width={['xs']}>
                            <h3>Load template</h3>
                        </GridColumn>
                    </Grid>
                </GridColumn>
                <GridColumn width={['xs-3']}>
                    <TextField
                        id="templateSelect"
                        select={true}
                        label="Choose template"
                        className="solvent__field"
                        SelectProps={{
                            MenuProps: {
                                className: 'solvent__field-select',
                            },
                        }}
                        margin="none"
                        onChange={handleChange}
                        value={props.selectedTemplate}
                    >
                        {props.templates.length ? props.templates.sort((a: ITemplate, b: ITemplate) => {
                            // sort alphabetically
                            if (a.name < b.name) { return -1; }
                            if (a.name > b.name) { return 1; }

                            return 0;
                        }).map((temp: ITemplate) => (
                            <MenuItem key={temp.id} value={temp.name}>
                                {temp.name}
                            </MenuItem>
                        )) : (
                                <MenuItem disabled={true}>
                                    No templates available
                                </MenuItem>
                            )}
                    </TextField>
                </GridColumn>
                <GridColumn width={['xs-3']}>
                    <Button text="Manage templates" color="ghost" onClick={onClickManage} />
                    <Modal
                        id="modal-example"
                        isOpen={isModalOpen}
                        onClose={handleModalClose}
                        size="sm"
                    >
                        <h3>Saved templates</h3>
                        <ul className="load-template__list">
                            {getTemplatesList()}
                        </ul>
                    </Modal>
                </GridColumn>
            </Grid>

        </div>
    );

}

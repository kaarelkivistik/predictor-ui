import React from 'react';

import Button from '@button';
import GridColumn from '@column';
import Grid from '@grid';
import TextField from '@material-ui/core/TextField';

if (process.env.webpack) {
    require('./save-template.scss');
}

export interface ISaveTemplateProps {
    onClickSave?: (name: string) => Promise<boolean | undefined>;
}

export interface ISaveTemplateSelectOption {
    label: string;
    value: string;
}

export default function SaveTemplate(props: ISaveTemplateProps): JSX.Element {
    const [templateName, setTemplateName] = React.useState('');

    const onTemplateNameChange: (e: React.ChangeEvent<HTMLInputElement>) => void = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTemplateName(e.target.value);
    };

    const onClickSave: () => void = async() => {
        if (props.onClickSave) {
            const success: boolean | undefined = await props.onClickSave(templateName);

            if (success) {
                setTemplateName('');
            }
        }
    };

    return (
        <div className="save-template">
            <Grid align={['start-xs']}>
                <GridColumn width={['xs-3']}>
                    <Grid align={['center-xs']}>
                        <GridColumn width={['xs']}>
                            <h3>Save template</h3>
                        </GridColumn>
                    </Grid>
                </GridColumn>
                <GridColumn width={['xs-3']}>
                    <TextField
                        id="templateName"
                        className="save-template__field"
                        label="Template name"
                        margin="none"
                        onChange={onTemplateNameChange}
                        value={templateName}
                    />
                </GridColumn>
                <GridColumn width={['xs-3']}>
                    <Button text="Save template" color="ghost" onClick={onClickSave} />
                </GridColumn>
            </Grid>
        </div>
    );

}

import React from 'react';

import classNames from 'classnames';

import GridColumn from '@column';
import Grid from '@grid';

import CircularProgress from '@material-ui/core/CircularProgress';

if (process.env.webpack) {
    require('./step-3.scss');
}

export interface IStep3Props {
    waitTime: string;
    className?: string;
}

export default class Step3 extends React.Component<IStep3Props, {}> {
    render(): JSX.Element {
        const className: string = classNames(
            'step-3',
            this.props.className,
        );

        return (
            <div className={className}>
                <Grid equalHeight={true} align={['middle-xs', 'center-xs']}>
                    <GridColumn width={['xs']}>
                        <div className="text text-center">
                            <CircularProgress />
                            <p>
                                Calculation in progress... <br />
                                Estimated calculation time <b>{this.props.waitTime}</b>
                            </p>
                        </div>
                    </GridColumn>
                </Grid>
            </div>
        );
    }
}

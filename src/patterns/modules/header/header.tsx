import React, { MutableRefObject, useEffect, useRef, useState } from 'react';

import classNames from 'classnames';

import GridColumn from '@column';
import Container from '@container';
import Grid from '@grid';
import Helpers from '@helpers';
import Icon from '@icon';
import Logo, { ILogoProps } from '@logo';
import Navigation, { INavigationItemProps, INavigationProps } from '@navigation';

if (process.env.webpack) {
    require('./header.scss');
}

export interface IHeaderProps {
    navigation: INavigationProps;
    logo?: ILogoProps;
}

const Header: React.FC<IHeaderProps> = (props: IHeaderProps) => {
    const inner: MutableRefObject<HTMLDivElement | null> = useRef(null);
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const [hasScrolled, setHasScrolled] = useState<boolean>(false);
    const className: string = classNames(
        'header',
        {
            'is-open': isOpen,
            'has-scrolled': hasScrolled,
        },
    );

    useEffect(() => {
        handleScroll();
        document.addEventListener('mousedown', onClickOutside);
        document.addEventListener('scroll', handleScroll);

        return () => {
            document.removeEventListener('mousedown', onClickOutside);
            document.removeEventListener('scroll', handleScroll);
        };
    });

    const handleScroll: () => void = () => {
        const offsetTop: number = window.pageYOffset;

        if (offsetTop >= 10) {
            if (!hasScrolled) {
                setHasScrolled(true);
            }
        } else {
            if (hasScrolled) {
                setHasScrolled(false);
            }
        }
    };

    const onClickOutside: (event: Event) => void = (event: Event) => {
        if (inner.current && !inner.current.contains(event.target as HTMLElement) && isOpen) {
            closeMenu();
        }
    };

    const closeMenu: () => void = () => {
        setIsOpen(false);
        setBodyScroll();
    };

    const setBodyScroll: () => void = () => {
        if (isOpen) {
            Helpers.disableScroll();
        } else {
            Helpers.enableScroll();
        }
    };

    const toggleMenu: () => void = () => {
        setIsOpen((prevState: boolean) => !prevState);
        setBodyScroll();
    };

    function setElementRef(element: HTMLDivElement | null): void {
        inner.current = element;
    }

    return (
        <header className={className}>
            <Container>
                <Grid align={['between-xs']}>
                    <GridColumn className="header__logo-container" width={['xs-4']}>
                        <Logo {...props.logo} className="header__logo" />
                    </GridColumn>
                    {props.navigation.items.length > 0 &&
                        (
                            <GridColumn className="header__toggle-container" width={['xs']}>
                                <button
                                    className="header__button-open"
                                    onClick={toggleMenu}
                                >
                                    <Icon name="menu" />
                                </button>
                            </GridColumn>
                        )
                    }
                    <GridColumn
                        className="header__mobile"
                        elementRef={setElementRef}
                        width={['xs']}
                        align={['middle-xs']}
                    >
                        <div className="header__top">
                            <button
                                className="header__button-close"
                                onClick={closeMenu}
                            >
                                <Icon name="close" />
                            </button>
                        </div>
                        <div className="header__bottom">
                            <Navigation
                                {...props.navigation}
                                items={props.navigation.items.map((item: INavigationItemProps) => {
                                    return {
                                        ...item,
                                        onClick: (e: React.MouseEvent<HTMLAnchorElement>) => {
                                            closeMenu();

                                            if (item.onClick) {
                                                item.onClick(e);
                                            }
                                        },
                                    };
                                })}
                            />
                        </div>
                    </GridColumn>
                </Grid>
            </Container>
        </header>
    );
};

export default Header;

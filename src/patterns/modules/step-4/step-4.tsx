import React from 'react';

import classNames from 'classnames';

import GridColumn from '@column';
import Grid from '@grid';

if (process.env.webpack) {
    require('./step-4.scss');
}

export interface IResult {
    fileName: string;
    url: string;
}

export interface IStep4Props {
    downloadLink: React.HTMLProps<HTMLAnchorElement>;
    success: boolean;
    className?: string;
}

export default class Step4 extends React.Component<IStep4Props, {}> {
    renderSuccessMessage(): JSX.Element {
        return (
            <div className="text">
                <p>Calculation has been finished!</p>

                <a {...this.props.downloadLink}>Click here</a> to download the results.
            </div>

        );
    }

    renderFailMessage(): JSX.Element {
        return (
            <p>
                Calculation failed
            </p>
        );
    }

    render(): JSX.Element {
        const className: string = classNames(
            'step-4',
            this.props.className,
        );

        return (
            <div className={className}>
                <Grid equalHeight={true} align={['middle-xs', 'center-xs']}>
                    <GridColumn width={['xs']}>
                        <div className="text text-center">
                            {this.props.success ? this.renderSuccessMessage() : this.renderFailMessage()}
                        </div>
                    </GridColumn>
                </Grid>
            </div>
        );
    }
}

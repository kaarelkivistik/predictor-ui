import React from 'react';

import classNames from 'classnames';

import GridColumn from '@column';
import FileDrop, { IFileDropProps } from '@file-drop';
import Grid from '@grid';

export interface IStep2Props {
    fileDrop: IFileDropProps;
    csvUrl?: string;
    className?: string;
}

export default class Step2 extends React.Component<IStep2Props, {}> {
    render(): JSX.Element {
        const className: string = classNames(
            'step-2',
            this.props.className,
        );

        return (
            <div className={className}>
                <Grid equalHeight={true} align={['middle-xs', 'center-xs']}>
                    <GridColumn width={['xs-6']}>
                        <Grid align={['center-xs']}>
                            <GridColumn width={['xs']}>
                                <div className="text text-center">
                                    <p>
                                        Download the sample .csv file from <a href={this.props.csvUrl} download={true}>here</a>
                                    </p>
                                </div>
                            </GridColumn>
                        </Grid>
                    </GridColumn>
                    <GridColumn width={['xs-6']}>
                        <FileDrop {...this.props.fileDrop} />
                    </GridColumn>
                </Grid>
            </div>
        );
    }
}

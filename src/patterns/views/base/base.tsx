import React from 'react';

import CookieNotice, { ICookieNoticeProps } from '@cookie-notice';
import Main from '@main';

import GlobalNotification from '@global-notification';

import Header, { IHeaderProps } from '@m-header';

import TemplateLoading from '@t-loading';

export interface IViewBaseProps {
    children: React.ReactNode;
    cookieNotice?: ICookieNoticeProps;
    header?: IHeaderProps;
    isAuthenticated?: boolean;
    isLoading?: boolean;
    elementRef?: React.Ref<HTMLDivElement>;
    error?: string;
}

const ViewBase: React.FC<IViewBaseProps> = (props: IViewBaseProps) => {
    const renderHeader: () => JSX.Element | null = () => {
        if (!props.header) {
            return null;
        }

        return (
            <Header {...props.header} />
        );
    };

    const renderContent: () => JSX.Element = () => {
        return (
            <React.Fragment>
                {props.header && renderHeader()}
                <Main elementRef={props.elementRef}>
                    {props.children}
                </Main>
                {props.cookieNotice && <CookieNotice {...props.cookieNotice} />}
                {props.error && <GlobalNotification message={props.error} />}
            </React.Fragment>
        );
    };

    return props.isLoading ? <TemplateLoading /> : renderContent();
};

export default ViewBase;

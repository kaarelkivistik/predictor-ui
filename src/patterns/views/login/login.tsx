import React from 'react';

import TemplateLogin, { ITemplateLoginProps } from '@t-login';
import ViewBase, { IViewBaseProps } from '@v-base';

export interface IViewLoginProps {
    base: IViewBaseProps;
    template: ITemplateLoginProps;
}

const ViewLogin: React.FC<IViewLoginProps> = (props: IViewLoginProps) => {
        return (
            <ViewBase {...props.base}>
                <TemplateLogin {...props.template} />
            </ViewBase>
        );
};

export default ViewLogin;

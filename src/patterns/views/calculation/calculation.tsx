import React from 'react';

import TemplateCalculation, { ITemplateCalculationProps } from '@t-calculation';
import ViewBase, { IViewBaseProps } from '@v-base';

export interface IViewCalculationProps {
    base: IViewBaseProps;
    template: ITemplateCalculationProps;
}

const ViewCalculation: React.FC<IViewCalculationProps> = (props: IViewCalculationProps) => {
    return (
        <ViewBase {...props.base}>
            <TemplateCalculation {...props.template} />
        </ViewBase>
    );
};

export default ViewCalculation;

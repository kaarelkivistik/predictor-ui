import React from 'react';

import Container from '@container';
import Progress, { IProgressProps } from '@progress';

export interface ITemplateCalculationProps {
    progress: IProgressProps;
}

const TemplateCalculation: React.FC<ITemplateCalculationProps> = (props: ITemplateCalculationProps) => {
    return (
        <Container>
            <Progress {...props.progress} />
        </Container>
    );
};

export default TemplateCalculation;

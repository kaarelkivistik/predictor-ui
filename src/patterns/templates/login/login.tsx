import React from 'react';

import Button, { IButtonProps } from '@button';
import GridColumn from '@column';
import Container from '@container';
import Grid from '@grid';
import Logo from '@logo';

if (process.env.webpack) {
    require('./login.scss');
}

export interface ITemplateLoginProps {
    button: IButtonProps;
}

const TemplateLogin: React.FC<ITemplateLoginProps> = (props: ITemplateLoginProps) => {
    return (
        <div className="login">
            <Container>
                <Grid align={['center-xs']}>
                    <GridColumn width={['xs']}>
                        <Logo className="login__logo" />
                    </GridColumn>
                </Grid>
                <Grid align={['center-xs']}>
                    <GridColumn width={['xs-12', 'sm-6', 'md-5']}>
                        <Grid align={['center-xs']}>
                            <GridColumn width={['xs-12']}>
                                <Grid noGutter="all" align={['center-xs']}>
                                    <GridColumn width={['xs']}>
                                        <Button {...props.button} iconLeft="google" color="ghost" />
                                    </GridColumn>
                                </Grid>
                            </GridColumn>
                        </Grid>
                    </GridColumn>
                </Grid>
            </Container>
        </div>
    );
};

export default TemplateLogin;

import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';

import GridColumn from '@column';
import Container from '@container';
import Grid from '@grid';
import Main from '@main';

if (process.env.webpack) {
    require('./loading.scss');
}

const TemplateLoading: React.FC = () => {
    return (
        <Main className="loading">
            <Container>
                <Grid align={['center-xs']}>
                    <GridColumn width={['xs']}>
                        <CircularProgress />
                    </GridColumn>
                </Grid>
            </Container>
        </Main>
    );
};

export default TemplateLoading;

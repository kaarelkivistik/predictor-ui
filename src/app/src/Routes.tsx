import React from 'react';

import { Router } from '@reach/router';

import useAuth from './hooks/useAuth';

import { IAuthContext } from './providers/AuthProvider';

import TemplateLoading from '@t-loading';

import BaseOffline from './views/BaseOffline';
import BaseOnline from './views/BaseOnline';
import Calculation from './views/Calculation';
import Login from './views/Login';

const publicRoutes: () => JSX.Element = () => {
    return (
        <Router component={BaseOnline}>
            <Login path="/" />
        </Router>
    );
};

const privateRoutes: () => JSX.Element = () => {
    return (
        <Router component={BaseOnline}>
            <Calculation path="/calculation" />
        </Router>
    );
};

export interface IRoutesProps {
    useAuth: boolean;
}

const Routes: React.FC<IRoutesProps> = (props: IRoutesProps) => {
    if (props.useAuth) {
        const auth: IAuthContext = useAuth();

        if (auth.isLoaded) {
            if (auth.isAuthenticated) {
                return privateRoutes();
            } else {
                return publicRoutes();
            }
        } else {
            return <TemplateLoading />;
        }
    } else {
        return (
            <Router component={BaseOffline}>
                <Calculation path="/" />
            </Router>
        );
    }
};

export default Routes;

import { useContext } from 'react';

import { AuthContext, IAuthContext } from '../providers/AuthProvider';

export default function useAuth(): IAuthContext {
    return useContext(AuthContext);
}

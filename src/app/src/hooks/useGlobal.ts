import { useContext } from 'react';

import { GlobalContext, IGlobalContext } from '../providers/GlobalProvider';

export default function useGlobal(): IGlobalContext {
    return useContext(GlobalContext);
}

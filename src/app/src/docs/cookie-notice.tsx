import React from 'react';

import { RouteComponentProps } from '@reach/router';

const CookieNotice: React.FC<RouteComponentProps> = () => {
    return (
        <div className="text">
            <h1 className="text-center">PRIVACY NOTICE</h1>
            <h2 className="text-center">Quantem Analytics</h2>
            <p className="text__margin-double">
                This privacy notice describes the principles of processing the personal data of the users and visitors of the website <a href="https://quantem.co" target="_blank">https://quantem.co</a> (<b>„Website“</b>).
            </p>
            <p>
                The controller of personal data is Quantem Analytics OÜ, registry code 14689092, address Tartu maakond, Tartu, Narva mnt 149-8, 51008 (<b>“Quantem”</b>).
            </p>
            <h3>What personal information we collect</h3>
            <p>
                During sign-up, we collect your name and e-mail address. The legal basis for processing this data is your consent
            </p>
            <p>
                In addition, Quantem processes personal data through the use of cookies and other similar technologies
            </p>
            <h3>For what purpose do we collect personal information</h3>
            <p>Quantem processes personal data for the following purposes:</p>
            <ul>
                <li>To keep you updated on our services;</li>
                <li>To provide you a better experience with our Website;</li>
                <li>To be able to communicate with you;</li>
                <li>To ensure and fulfill the terms of service;</li>
                <li>To develop and improve our services</li>
            </ul>
            <h3>Access and transfer of personal data</h3>
            <p>
                Quantem employees have access to personal data to the extent necessary for the performance of their duties
            </p>
            <p>
                Your personal data may be transferred to third parties (for example, technical support) if this is strictly necessary for the provision of our services and the Website. If personal data is shared with third parties through the Website (including Facebook, Google), then Quantem is not responsible for third party data protection practices. We recommend that you review the relevant third-party privacy notices.
            </p>
            <h3>Security</h3>
            <p>
                Personal data is stored on servers located in the European Union. Personal data may be transferred to third parties in the United States but only to companies that have signed up to the Privacy Shield agreement, which provides a sufficient level of security.
            </p>
            <p>
                Quantem has implemented appropriate technical and organizational measures to ensure the security of personal data, including unauthorized or unlawful processing, and accidental loss, destruction or damage of personal data.
            </p>
            <p>
                If a personal data breach occurs and such violation may pose a high risk to your privacy, Quantem informs you and the relevant authorities of such breach.
            </p>
            <h3>Rights</h3>
            <p>
                You have the right to request the rectification, access and, if necessary, objection to the processing of personal data.
            </p>
            <p>
                You may request the deletion of personal data, after which we will inform you about the terms and time limit for deleting the personal data. The personal data will not be deleted if there is a legal basis for its retention.
            </p>
            <p>
                You have the right to receive personal data in machine-readable form and to transfer such data to another data processor.
            </p>
            <p>
                You have the right to withdraw your consent to processing personal data at any time.
            </p>
            <p>
                In order to exercise your rights on personal data, please contact Quantem at <a href="mailto:info@quantem.co">info@quantem.co</a>.
            </p>
            <p>
                You also have the right to appeal to your local Data Protection Inspectorate.
            </p>
            <h3>Retention</h3>
            <p>
                Your personal data shall be retained until you ask to delete your personal data. After some time, we may renew your consent in order to keep it active and valid.  Personal data may be stored in anonymised form for statistical and analytical purposes.
            </p>
            <p>
                In the event of a dispute, the personal data will be retained until an agreement is reached or until the due date for the claim expires.
            </p>
            <h3>Direct marketing</h3>
            <p>
                If you have given your consent, Quantem may send direct marketing messages to your email address. You can opt out of direct marketing messages at any time by contacting us.
            </p>
            <h3>Miscellaneous</h3>
            <p>
                In case of a request, application or complaint concerning the processing of personal data, please contact <a href="mailto:info@quantem.co">info@quantem.co</a>.
            </p>
            <p>
                Quantem has the right to change this privacy notice at any time. Quantem will notify you of any changes to the privacy notice on its webpage.
            </p>
        </div>
    );
};

export default CookieNotice;

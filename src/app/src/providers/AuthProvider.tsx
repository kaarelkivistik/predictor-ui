import React, { Context, createContext, useEffect, useState } from 'react';

import { navigate } from '@reach/router';
import Axios, { AxiosError, AxiosResponse } from 'axios';

import { ACCESS_TOKEN, API_BASE_URL, GOOGLE_AUTH_URL } from '../constants';

import Helpers from '@helpers';

interface IAuthContextBase {
    isLoaded: boolean;
    signIn: () => void;
    signOut: () => void;
    error?: string;
}

export interface IUnAuthenticatedContext extends IAuthContextBase {
    isAuthenticated: false;
}

export interface IAuthenticatedContext extends IAuthContextBase {
    isAuthenticated: true;
    user: AxiosResponse;
}

export type IAuthContext = IAuthenticatedContext | IUnAuthenticatedContext;

export const AuthContext: Context<IAuthContext> = createContext<IAuthContext>({
    isLoaded: false,
    isAuthenticated: false,
    signIn: () => null,
    signOut: () => null,
});

export interface IAuthProviderProps {
    children?: React.ReactNode;
}

const AuthProvider: React.FC<IAuthProviderProps> = (props: IAuthProviderProps) => {
    const [user, setUser] = useState<AxiosResponse | null>(null);
    const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [error, setError] = useState<string | undefined>(undefined);

    useEffect(() => {
        authenticate();

         // Refresh token every 5 min
        const interval: number = window.setInterval(authenticate, 5 * 60 * 1000);

        return () => window.clearInterval(interval);
    }, []);

    // tslint:disable: no-any
    // tslint:disable:typedef
    const request: (options: any) => object = (options: any) => {
        const headers: Headers = new Headers({
            'Content-Type': 'application/json',
        });

        if (localStorage.getItem(ACCESS_TOKEN)) {
            headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN));
        }

        const defaults: {headers: Headers} = {headers: headers};
        // tslint:disable-next-line: no-parameter-reassignment
        options = {...defaults, ...options};

        return fetch(options.url, options).then((response: Response) =>
            response.json().then((json: object) => {
                if (!response.ok) {
                    return Promise.reject(json);
                }

                return json;
            }),
        );
    };

    const getCurrentUser: () => any = () => {
        if (Helpers.getCookie('XSRF-TOKEN') === undefined) {
            return Promise.reject('Access token not set.');
        }

        return request({
            url: API_BASE_URL + '/user/me',
            method: 'GET',
        });
    };
    // tslint:enable: no-any
    // tslint:enable:typedef

    const authenticate: () => void = (): void => {
        getCurrentUser().then((response: AxiosResponse) => {
            setIsAuthenticated(true);
            setUser(response);
            setIsLoaded(true);
            navigate('/calculation').catch(setError);
        }).catch((e: AxiosError) => {
            // Prevent 'Failed to fetch' error from showing if unauthenticated
            if (e.message !== 'Failed to fetch') {
                setError(e.message);
            }
            setIsLoaded(true);
        });
    };

    const signIn: () => void = (): void => {
        Axios.post(GOOGLE_AUTH_URL).catch(setError);
    };

    const signOut: () => void = (): void => {
        Axios.post('/api/logout').catch(setError).finally(() => {
            setIsAuthenticated(false);
            setUser(null);
            navigate('/').catch(setError);
            localStorage.removeItem(ACCESS_TOKEN);
            setError('Logged out');
        }).catch(setError);
    };

    const getValue: () => IAuthContext = (): IAuthContext => {
        if (isAuthenticated && user) {
            return {
                error,
                isAuthenticated: true,
                isLoaded,
                signIn,
                signOut,
                user,
            };
        } else {
            return {
                error,
                isAuthenticated: false,
                isLoaded,
                signIn,
                signOut,
            };
        }
    };

    return (
        <AuthContext.Provider value={getValue()}>
            {props.children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;

import React, { Context, createContext, useEffect, useState } from 'react';

import { globalHistory, Location } from '@reach/router';

export interface IGlobalProviderProps {
    children?: React.ReactNode;
}

export interface IGlobalContext {
    headerOpen: boolean;
    setHeaderOpen: (open: boolean) => void;
    error?: string;
    setError: (e: string | undefined) => void;
}

export const GlobalContext: Context<IGlobalContext> = createContext<IGlobalContext>({
    headerOpen: false,
    setHeaderOpen: () => null,
    setError: () => null,
});

const GlobalProvider: React.FC<IGlobalProviderProps> = (props: IGlobalProviderProps) => {
    const [headerOpen, setHeaderOpen] = useState<boolean>(false);
    const [error, setError] = useState<string | undefined>(undefined);

    const value: IGlobalContext = {
        error,
        headerOpen,
        setError,
        setHeaderOpen,
    };

    useEffect(() => {
        globalHistory.listen(() => setHeaderOpen(false));
    }, []);

    return (
        <Location>
            {() => {

                return (
                    <GlobalContext.Provider value={value}>
                        {props.children}
                    </GlobalContext.Provider>
                );
            }}
        </Location>
    );
};

export default GlobalProvider;

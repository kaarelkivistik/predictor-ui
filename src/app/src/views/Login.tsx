import React from 'react';

import { RouteComponentProps } from '@reach/router';
import {
    GOOGLE_AUTH_URL,
} from '../constants';

import TemplateLogin, { ITemplateLoginProps } from '@t-login';

export default class Login extends React.Component<RouteComponentProps> {
    render(): JSX.Element {
        const loginProps: ITemplateLoginProps = {
            button: {
                text: 'Log in with Google',
                url: GOOGLE_AUTH_URL,
            },
        };

        return (
            <TemplateLogin {...loginProps} />
        );
    }
}

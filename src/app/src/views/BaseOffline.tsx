import React from 'react';

import { IHeaderProps } from '@m-header';

import ViewBase from '@v-base';

export interface IBaseOfflineProps {
    children?: React.ReactNode;
}

const BaseOffline: React.FC<IBaseOfflineProps> = React.forwardRef((props: IBaseOfflineProps, ref: React.Ref<HTMLDivElement>) => {
    function getHeaderProps(): IHeaderProps {
        return {
            navigation: {
                items: [],
            },
        };
    }

    return (
        <ViewBase
            children={props.children}
            header={getHeaderProps()}
            elementRef={ref}
        />
    );
});

export default BaseOffline;

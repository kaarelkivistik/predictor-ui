import React from 'react';

import { withTranslation, WithTranslation } from 'react-i18next';
import useAuth from '../hooks/useAuth';

import CookieNotice from '../docs/cookie-notice';

import { ICookieNoticeProps } from '@cookie-notice';
import { INavigationItemProps } from '@navigation';

import { IHeaderProps } from '@m-header';

import ViewBase from '@v-base';

export interface IBaseOnlineProps {
    children?: React.ReactNode;
}

const BaseOnline: React.FC<IBaseOnlineProps & WithTranslation> = React.forwardRef((props: IBaseOnlineProps & WithTranslation, ref: React.Ref<HTMLDivElement>) => {
    const { error, isAuthenticated, signOut } = useAuth();
    const currentPath: string = window.location ? window.location.pathname : '/';

    const getNavigationItems: () => INavigationItemProps[] = () => {
        return [
            {
                url: 'https://quantem.co/wp-content/uploads/2020/02/User_manual_online.pdf',
                label: props.t('navigation.how_to'),
                onClick: (event: React.MouseEvent) => {
                    event.preventDefault();

                    const win: Window | null = window.open(
                        'https://quantem.co/wp-content/uploads/2020/02/User_manual_online.pdf',
                        '_blank',
                    );
                    win && win.focus();
                },
            },
            {
                url: '/calculation',
                label: props.t('navigation.calculation'),
                onClick: (event: React.MouseEvent) => {
                    event.preventDefault();

                    if (getIsCurrent('/calculation')) {
                        location.reload();
                    }
                },
            },
            {
                url: '/api/logout',
                label: props.t('navigation.log_out'),
                onClick: (event: React.MouseEvent) => {
                    event.preventDefault();

                    signOut();
                },
            },
            {
                url: '/history',
                label: props.t('navigation.history'),
                disabled: true,
            },
        ].map((item: INavigationItemProps): INavigationItemProps => {
            const isCurrent: boolean = item.url === '/' ? item.url === currentPath : currentPath.indexOf(item.url) === 0;

            return {
                ...item,
                current: isCurrent,
            };
        });
    };

    function getIsCurrent(url: string): boolean {
        return url === '/' ? url === currentPath : currentPath.indexOf(url) === 0;
    }

    function getHeaderProps(): IHeaderProps {
        return {
            logo: {
                url: 'https://quantem.co',
            },
            navigation: {
                items: isAuthenticated ? getNavigationItems() : [],
            },
        };
    }

    function getCookieNoticeProps(): ICookieNoticeProps {
        return {
            acceptButton: {
                text: 'I accept cookies from this site',
            },
            moreButton: {
                text: 'Find out more',
            },
            modalContent: <CookieNotice />,
            children: getCookieContent(),
        };
    }

    function getCookieContent(): JSX.Element {
        return (
            <div className="text">
                <h2>Cookies on Quantem Analytics</h2>
                <p>
                    The Quantem Analytics website employs cookies to improve your user
                    experience. We have updated our cookie policy to reflect changes in the
                    law on cookies and tracking technologies used on website. If you continue
                    on this website, you will be providing your consent to our use of cookies.
                </p>
            </div>
        );
    }

    return (
        <ViewBase
            children={props.children}
            cookieNotice={getCookieNoticeProps()}
            header={getHeaderProps()}
            elementRef={ref}
            error={error}
        />
    );
});

export default withTranslation('translation')(BaseOnline);

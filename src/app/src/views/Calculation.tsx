import React from 'react';

import { RouteComponentProps } from '@reach/router';
import Axios, { AxiosError, AxiosResponse } from 'axios';
import { withTranslation, WithTranslation } from 'react-i18next';
import {
    API_BASE_URL,
} from '../constants';

import { IButtonProps } from '@button';
import { TChoiceGroupValue } from '@choice-group';
import { IParsedFile } from '@file-drop';
import { IGradientProps } from '@gradient';
import { IGradientItemProps, IGradientItemState } from '@gradient-item';
import Helpers from '@helpers';
import { IModeProps, TEsiMode } from '@mode';
import { IProgressProps } from '@progress';
import { ISolventProps, ISolventSelectProps, ISolventState } from '@solvent';

import { IStep2Props } from '@m-step-2';
import { IStep3Props } from '@m-step-3';
import { IStep4Props } from '@m-step-4';

import { ITemplate } from '@m-load-template';
import TemplateCalculation from '@t-calculation';
import { downloadAndZip } from '../plugins/jszip';

export interface IResult {
    fileName: string;
    url: string;
}

export interface ICalcResponseData {
    status: 'FINISHED' | 'ERROR' | 'RUNNING';
    message: string;
    url: string;
    inputFileName: string;
}
export interface ICalculationState {
    activeStep: number;
    esiMode: TEsiMode;
    mode: IModeProps;
    isMounted: boolean;
    waterOptions: ISolventSelectProps[];
    organicOptions: ISolventSelectProps[];
    water?: string;
    organic?: string;
    ph?: string;
    items: IGradientItemProps[];
    values: IGradientItemState[];
    calculationCode?: string;
    files?: IParsedFile[];
    waitTime?: number;
    sampleUrl?: string;
    calculationFinished: boolean;
    resultsUrl?: string;
    results: IResult[];
    failures?: ICalcResponseData[];
    error?: string;
    success?: string;
    fileName?: string;
    templates: ITemplate[];
    selectedTemplate: string;
}

interface IQueryGradients {
    startTime: number;
    organicPhaseModifierPercent: number;
}

export class Calculation extends React.Component<RouteComponentProps & WithTranslation, ICalculationState> {
    static defaultItem: IGradientItemProps[] = [
        {
            index: 0,
            id: 'gradient-item-1',
            time: '0',
        },
    ];

    interval: number = 0;
    defaultCalculationState: ICalculationState = {
        activeStep: 0,
        esiMode: 'positive',
        mode: {
            title: this.props.t('labels.esi_mode'),
            choiceGroup: {
                label: this.props.t('labels.esi_mode'),
                type: 'radio',
                choices: [
                    {
                        id: 'check1',
                        label: this.props.t('labels.positive'),
                        name: 'check',
                        value: 'positive',
                        checked: true,
                    },
                    {
                        id: 'check2',
                        label: this.props.t('labels.negative'),
                        name: 'check',
                        value: 'negative',
                        checked: false,
                    },
                ],
                onChange: (value: TChoiceGroupValue): void => {
                    this.setState({
                        esiMode: value as TEsiMode,
                    });
                },
            },
        },
        isMounted: false,
        calculationFinished: false,
        waterOptions: [
            {
                value: '',
                label: '',
            },
        ],
        organicOptions: [
            {
                value: '',
                label: '',
            },
        ],
        items: Calculation.defaultItem,
        ph: '7',
        values: [
            {
                time: '0',
                water: '100',
                organic: '0',
            },
        ],
        results: [],
        templates: [],
        selectedTemplate: '',
    };

    state: ICalculationState = this.defaultCalculationState;

    componentDidMount(): void {
        this.setState({
            isMounted: true,
            waterOptions: [],
        }, () => {
            this.getWaterOptions();
            this.getCsvSampleUrl();
            this.getTemplates();
        });
    }

    getGradientSteps(): IQueryGradients[] {
        return this.state.values.map((value: IGradientItemState) => {
            return ({
                startTime: parseInt(value.time, 10),
                organicPhaseModifierPercent: parseInt(value.organic, 10),
            });
        });
    }

    async saveTemplate(name: string): Promise<boolean | undefined> {
        if (this.validateFields()) {
            const steps: IQueryGradients[] = this.getGradientSteps();

            const data: string = JSON.stringify({
                ESImode: this.state.esiMode,
                organicModifier: this.state.organic,
                waterPhaseAdditive: this.state.water,
                waterPhaseAdditivePercent: null,
                pH: this.state.ph,
                steps: steps,
            });
            const success: boolean | undefined = await Axios.post(API_BASE_URL + '/predictor/eluent/template/' + name, data, {
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then(async (_: AxiosResponse) => {
                    this.getTemplates();
                    this.notifySuccess('Template saved');
                    this.setState({ selectedTemplate: name });

                    return true;
                })
                .catch((error: AxiosError) => {
                    if (error.response && error.response.data.message) {
                        this.notifyError(error.response.data.message);

                        return false;
                    }
                });

            return success;
        }

        return false;
    }

    getTemplates(): void {
        Axios.get(API_BASE_URL + '/predictor/eluent/templates')
            .then((res: AxiosResponse) => {

                this.setState({
                    templates: res.data,
                }, () => {
                    if (this.state.selectedTemplate === '') {
                        // Select default template
                        this.selectTemplate();
                    }
                });
            })
            .catch(this.handleResponseErrors);
    }

    selectTemplate(name?: string): void {
        const template: ITemplate | undefined = name ? this.state.templates.find((temp: ITemplate) => temp.name === name)
            : this.state.templates.find((temp: ITemplate) => temp.default);

        if (template) {
            const values: IGradientItemState[] = template.gradientData.steps.map((step: { startTime: number, organicPhaseModifierPercent: number; }) => ({
                time: String(step.startTime),
                organic: String(step.organicPhaseModifierPercent),
                water: String(100 - step.organicPhaseModifierPercent),
            }));

            this.setState({
                esiMode: template.gradientData.ESImode,
                organic: template.gradientData.organicModifier,
                water: template.gradientData.waterPhaseAdditive,
                ph: template.gradientData.pH,
                values,
                selectedTemplate: template.name,
            });
        }
    }

    removeTemplate(name: string): void {
        Axios.delete(API_BASE_URL + '/predictor/eluent/template/' + name)
            .then((_: AxiosResponse) => {
                this.getTemplates();
                if (name === this.state.selectedTemplate) {
                    this.setState({ selectedTemplate: '' });
                }
            })
            .catch((error: AxiosError) => {
                if (error.response && error.response.data.message) {
                    this.notifyError(error.response.data.message);
                }
            });
    }

    setDefaultTemplate(name: string): void {
        Axios.put(API_BASE_URL + '/predictor/eluent/templates/default/' + name)
            .then((_: AxiosResponse) => {
                this.getTemplates();
            })
            .catch((error: AxiosError) => {
                if (error.response && error.response.data.message) {
                    this.notifyError(error.response.data.message);
                }
            });
    }

    notifyError(message: string): void {
        this.setState({
            error: message,
        }, () => {
            setTimeout(() => {
                this.setState({
                    error: undefined,
                });
            }, 5000);
        });
    }

    notifySuccess(message: string): void {
        this.setState({
            success: message,
        }, () => {
            setTimeout(() => {
                this.setState({
                    success: undefined,
                });
            }, 5000);
        });
    }

    getProgress = (): IProgressProps => {
        return {
            activeStep: this.state.activeStep,
            mode: { ...this.state.mode, mode: this.state.esiMode },
            solvent: this.getSolvent(),
            gradient: this.getGradient(),
            handleSolventChange: this.handleSolventChange,
            optionalLabel: this.props.t('labels.optional'),
            step2: this.getStep2(),
            step3: this.getStep3(),
            step4: this.getStep4(),
            steps: [
                {
                    label: this.props.t('calculation.steps.data_input'),
                },
                {
                    label: this.props.t('calculation.steps.csv_upload'),
                },
                {
                    label: this.props.t('calculation.steps.calculation'),
                },
                {
                    label: this.props.t('calculation.steps.completed'),
                },
            ],
            resetButton: {
                text: this.props.t('buttons.restart'),
            },
            backButton: {
                text: this.props.t('buttons.back'),
            },
            skipButton: {
                text: this.props.t('buttons.skip'),
            },
            finishButton: this.getFinishButton(),
            nextButton: {
                text: this.props.t('buttons.next'),
            },
            onResetClick: this.onResetClick,
            onPrevClick: this.onPrevClick,
            onNextClick: this.onNextClick,
            onStepChange: this.onStepChange,
            error: this.state.error,
            success: this.state.success,
            loadTemplate: {
                templates: this.state.templates,
                onSelect: (name: string) => this.selectTemplate(name),
                onRemove: (name: string) => this.removeTemplate(name),
                onSetDefault: (name: string) => this.setDefaultTemplate(name),
                selectedTemplate: this.state.selectedTemplate,
            },
            saveTemplate: {
                onClickSave: (name: string) => this.saveTemplate(name),
            },
        };
    }

    getFinishButton(): IButtonProps {
        if (this.state.results.length === 1) {
            return {
                text: this.props.t('buttons.save_results'),
                url: this.state.results[0].url,
            };
        } else {
            return {
                text: this.props.t('buttons.save_results'),
                onClick: () => downloadAndZip(this.state.results),
            };
        }
    }

    onStepChange = (step: number): void => {
        this.setState({
            activeStep: step,
        });
    }

    handleSolventChange = (name: keyof ISolventState, value: string): void => {
        // tslint:disable-next-line:no-object-literal-type-assertion
        this.setState({
            [name]: value,
        } as Pick<ISolventState, keyof ISolventState>);
    }

    onGradientChange = (name: keyof IGradientItemState, value: string, index: number): void => {
        this.setState((prevState: ICalculationState) => {
            const values: IGradientItemState[] = prevState.values.map((list: IGradientItemState, j: number) => {
                if (j === index) {
                    const nextList: IGradientItemState = {
                        ...list,
                        [name]: value,
                    };

                    return nextList;
                } else {
                    return list;
                }
            });

            return {
                values,
            };
        });
    }

    onGradientAddRemove = (values: IGradientItemState[]): void => {
        this.setState({
            values,
        });
    }

    getSolvent = (): ISolventProps => {
        return {
            title: this.props.t('labels.solvent'),
            waterTitle: this.props.t('labels.water_phase_additive'),
            waterOptions: this.state.waterOptions,
            organicTitle: this.props.t('labels.organic_phase'),
            organicOptions: this.state.organicOptions,
            water: this.state.water,
            organic: this.state.organic,
            ph: this.state.ph,
        };
    }

    getWaterOptions = (): void => {
        Axios.get(API_BASE_URL + '/predictor/eluent')
            .then((res: AxiosResponse) => {
                this.setState({
                    waterOptions: res.data.waterPhaseAdditives,
                    organicOptions: res.data.organicPhaseModifiers,
                });
            })
            .catch(this.handleResponseErrors);
    }

    onResetClick = (): void => {
        // this.setState({
        //     activeStep: 0,
        //     calculationCode: undefined,
        //     csv: undefined,
        //     calculationFinished: false,
        //     hasFailed: false,
        //     resultsUrl: undefined,
        //     waitTime: undefined,
        // }, () => this.getWaterOptions());

        location.reload();
    }

    onPrevClick = (): void => {
        if (this.state.isMounted) {
            this.setState({
                activeStep: this.state.activeStep - 1,
            });
        }
    }

    validateFields = (): boolean => {
        if (this.state.activeStep === 0) {
            if (this.state.organic === undefined) {
                this.notifyError(this.props.t('errors.please_select_organic_phase'));

                return false;
            }

            return true;
        } else {
            return true;
        }
    }

    onNextClick = (): void => {
        if (this.state.isMounted) {
            if (this.state.activeStep === 0) {
                if (this.validateFields()) {
                    const steps: IQueryGradients[] = this.getGradientSteps();

                    const data: string = JSON.stringify({
                        ESImode: this.state.esiMode,
                        organicModifier: this.state.organic,
                        waterPhaseAdditive: this.state.water,
                        waterPhaseAdditivePercent: null,
                        pH: this.state.ph,
                        steps: steps,
                    });

                    Axios.post(API_BASE_URL + '/predictor/eluent', data, {
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                        .then((res: AxiosResponse) => {
                            if (typeof res.data.calculationCode !== 'undefined') {
                                this.setState({
                                    activeStep: this.state.activeStep + 1,
                                    calculationCode: res.data.calculationCode,
                                });
                            } else {
                                alert(this.props.t('errors.check_onput') + '. ' + this.props.t('errors.could_not_fetch_calculation_id'));
                            }
                        })
                        .catch(this.handleResponseErrors);
                }
            } else if (this.state.activeStep === 1) {
                if (this.state.calculationCode && this.state.files && this.state.files.length) {
                    const data: string = JSON.stringify(this.state.files.map((file: IParsedFile) => ({ inputFileName: file.fileName, dataPoints: file.data })));

                    Axios.post(API_BASE_URL + '/predictor/upload/json/' + this.state.calculationCode.toString(), data, {
                        headers: {
                            'Content-Type': 'application/json',
                        },
                    })
                        .then((res: AxiosResponse) => {
                            if (typeof res.data.waitTimeMillis !== 'undefined') {
                                this.setState({
                                    activeStep: this.state.activeStep + 1,
                                    waitTime: res.data.waitTimeMillis,
                                }, () => {
                                    setTimeout(this.checkIfCalculationFinished, this.state.waitTime);
                                });

                            } else {
                                alert(this.props.t('errors.could_not_fetch_wait_time'));
                            }
                        })
                        .catch(this.handleResponseErrors);
                }
            }
        }
    }

    getGradient = (): IGradientProps => {
        return {
            title: this.props.t('labels.gradient'),
            values: this.state.values,
            handleAddClick: this.onGradientAddRemove,
            handleRemoveClick: this.onGradientAddRemove,
            onChange: this.onGradientChange,
        };
    }

    handleCsvUpload = (result: IParsedFile[]): void => {
        this.setState({
            files: result,
        });
    }

    getStep2 = (): IStep2Props => {
        return {
            csvUrl: this.state.sampleUrl,
            fileDrop: {
                label: this.props.t('labels.select_csv'),
                activeLabel: this.props.t('labels.drop_here'),
                changeHandle: this.handleCsvUpload,
            },
        };
    }

    getStep3 = (): IStep3Props => {
        return {
            waitTime: this.state.waitTime ? Helpers.millisToTime(this.state.waitTime) : '',
        };
    }

    getStep4 = (): IStep4Props => {
        const success: boolean = this.state.results.length > 0;
        if (this.state.results.length === 1) {
            return {
                downloadLink: {
                    href: this.state.results[0].url,
                },
                success,
            };
        } else {
            return {
                downloadLink: {
                    onClick: () => downloadAndZip(this.state.results),
                },
                success,
            };
        }
    }

    getCsvSampleUrl = (): void => {
        Axios.get(API_BASE_URL + '/predictor/upload/csv/example')
            .then((res: AxiosResponse) => {
                this.setState({
                    sampleUrl: res.config.url,
                });
            })
            .catch((error: AxiosError) => {
                // tslint:disable-next-line: no-console
                console.log(error);
            });
    }

    checkIfCalculationFinished = (): void => {
        this.interval = window.setInterval(() => {
            if (!this.state.calculationFinished && this.state.calculationCode) {
                Axios.get(API_BASE_URL + '/predictor/results/url/' + this.state.calculationCode)
                    .then((res: AxiosResponse) => {
                        this.setState({
                            calculationFinished: res.data.every((calc: ICalcResponseData) => calc.status === 'FINISHED' || calc.status === 'ERROR' || calc.message),
                        }, () => {
                            if (this.state.calculationFinished) {
                                const failures: ICalcResponseData[] = res.data.map((calc: ICalcResponseData) => calc.status === 'ERROR' && calc);
                                const results: IResult[] = res.data.map((calc: ICalcResponseData) => ({ url: API_BASE_URL + calc.url, fileName: calc.inputFileName }));
                                clearInterval(this.interval);

                                this.setState({
                                    activeStep: 3,
                                    results,
                                    failures,
                                }, () => {
                                    if (failures && length > 0) {
                                        let message: string = '';
                                        failures.forEach((failure: ICalcResponseData) => message += `${failure}\n`);
                                        this.notifyError(message);
                                    }
                                });
                            }
                        });
                    })
                    .catch(this.handleResponseErrors);
            }
        }, 5000);
    }

    handleResponseErrors = (error: AxiosError): void => {
        if (error.response && error.response.data.message) {
            this.notifyError(error.response.data.message);
        } else {
            // tslint:disable-next-line: no-console
            console.log(error);
        }
    }

    render(): JSX.Element {
        return (
            <TemplateCalculation progress={this.getProgress()} />
        );
    }
}

export default withTranslation('translation')(Calculation);

import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';

import I18NextXhrBackend from 'i18next-xhr-backend';

i18next
// load translation using xhr -> see /public/locales
// learn more: https://github.com/i18next/i18next-xhr-backend
    .use(I18NextXhrBackend)
    // pass the i18n instance to react-i18next.
    .use(initReactI18next)
    // init i18next
    // for all options read: https://www.i18next.com/overview/configuration-options
    .init({
        lng: process.env.LOCALE,
        fallbackLng: 'en',
        debug: true,
        // special options for react-i18next
        // learn more: https://react.i18next.com/components/i18next-instance
        backend: {
            loadPath: `./locales/{{lng}}.json?version=${new Date().getTime()}`,
        },
        react: {
            wait: true,
        },
        interpolation: {
            escapeValue: false, // react already safes from xss
        },
        returnObjects: true,
    })
    .catch(() => {
        // if needed we can display an error if there are some problems with translations
    });

export default i18next;

import { saveAs } from 'file-saver';
import JSZip from 'jszip';

interface ISource { fileName: string; url: string; }
interface IFileContent { fileName: string; blob: Promise<Blob>; }

const downloadMany: (sources: ISource[]) => Promise<IFileContent[]> = (sources: ISource[]): Promise<IFileContent[]> => {
    return Promise.all(sources.map((source: ISource) => fetch(source.url).then((res: Response) => ({
        fileName: source.fileName,
        blob: res.blob(),
    }))));
};

const exportZip: (files: IFileContent[]) => void = (files: IFileContent[]) => {
    const zip: JSZip = JSZip();
    const currentDate: string = new Date().toISOString();

    files.forEach(({ blob, fileName }: IFileContent) => {
        zip.file(`quantem_result_${currentDate}_${fileName}`, blob);
    });
    zip.generateAsync({ type: 'blob' }).then((zipFile: Blob) => {
        const fileName: string = `quantem-${currentDate}.zip`;

        return saveAs(zipFile, fileName);
    });
};

export const downloadAndZip: (sources: ISource[]) => void = (source: ISource[]) => {
    return downloadMany(source).then(exportZip);
};

export const API_BASE_URL: string = '//slhitesap906:8080';
export const ACCESS_TOKEN: string = 'accessToken';

export const OAUTH2_REDIRECT_URI: string = '//slhitesap906:80/oauth2/redirect';

export const GOOGLE_AUTH_URL: string = API_BASE_URL + '/oauth2/authorization/google?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const FACEBOOK_AUTH_URL: string = API_BASE_URL + '/oauth2/authorization/facebook?redirect_uri=' + OAUTH2_REDIRECT_URI;
export const GITHUB_AUTH_URL: string = API_BASE_URL + '/oauth2/authorize/github?redirect_uri=' + OAUTH2_REDIRECT_URI;

// POLYFILLS
import 'core-js'; // <- at the top of your entry point

import 'classlist-polyfill';
import 'custom-event-polyfill';
import 'element-scroll-polyfill';
import 'formdata-polyfill';
import 'react-app-polyfill/ie11';
import 'svgxuse';
import 'whatwg-fetch';

import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';

import '@helpers';
import '@reset';
import '@typography';

import Routes from './src/Routes';

import GlobalProvider from './src/providers/GlobalProvider';
import I18nProvider from './src/providers/I18nProvider';

import TemplateLoading from '@t-loading';

const App: React.FC = () => {
    return (
        <Suspense fallback={<TemplateLoading />}>
            <I18nProvider>
                <GlobalProvider>
                    <Routes useAuth={false} />
                </GlobalProvider>
            </I18nProvider>
        </Suspense>
    );
};

ReactDOM.render(<App />, document.getElementById('app'));
